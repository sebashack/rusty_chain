use chrono::{DateTime, Utc};
use futures::prelude::*;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::process::Command as ProcCommand;
use std::sync::Arc;
use tokio::io;
use tokio::io::{AsyncWriteExt, ReadHalf, WriteHalf};
use tokio::net::{UnixListener, UnixStream};
use tokio::sync::mpsc::{Receiver, Sender};
use tokio::sync::{Mutex, MutexGuard};
use tokio::task::JoinHandle;
use tokio_serde::formats::{Json, SymmetricalJson};
use tokio_serde::Framed;
use tokio_util::codec::{FramedRead, FramedWrite, LengthDelimitedCodec};

use super::blockchain::{
    AccountId, Block, BlockChain, ChainSyncError, Transaction, TransactionError, MAX_COINS,
};

static POLL_DELAY: u64 = 1000;

pub type Serializer = Framed<
    FramedWrite<WriteHalf<UnixStream>, LengthDelimitedCodec>,
    Value,
    Value,
    Json<Value, Value>,
>;
pub type Deserializer = Framed<
    FramedRead<ReadHalf<UnixStream>, LengthDelimitedCodec>,
    Value,
    Value,
    Json<Value, Value>,
>;
pub type Connection = (Serializer, JoinHandle<()>);
pub type Connections = (HashMap<NodeId, Connection>, Sender<Message>);
type ChainLength = usize;

pub struct Node {
    id: NodeId,
    blockchain: BlockChain,
    inbound_receiver: Receiver<Message>,
    outbound_sender: Sender<NetCommand>,
    longest_peer_chain: (NodeId, ChainLength),
    connections: Arc<Mutex<Connections>>,
    socker_dir: PathBuf,
}

#[derive(Serialize, Deserialize, Hash, PartialEq, Eq, Clone, Copy, Debug)]
pub struct NodeId(pub u16);

#[derive(Serialize, Deserialize, Debug)]
pub enum Message {
    UCommand(UserCommand),
    NCommand(NetCommand),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum UserCommand {
    SyncWith {
        peer_id: NodeId,
    },
    EchoTo {
        peer_id: NodeId,
        message: String,
    },
    MakeTransaction {
        from: AccountId,
        to: AccountId,
        amount: u32,
    },
    Mine,
    GetBalance {
        account_id: AccountId,
    },
}

#[derive(Serialize, Deserialize, Debug)]
pub enum NetCommand {
    ChainLengthReq {
        from: NodeId,
        to: NodeId,
    },
    ChainLengthRes {
        from: NodeId,
        to: NodeId,
        chain_length: usize,
    },
    SyncWithReq {
        from: NodeId,
        to: NodeId,
    },
    SyncWithRes {
        from: NodeId,
        to: NodeId,
        known_peers: Vec<NodeId>,
        chain: Vec<Block>,
    },
    EchoToReq {
        from: NodeId,
        to: NodeId,
        message: String,
    },
    EchoToRes {
        from: NodeId,
        to: NodeId,
        message: String,
    },
}

impl Node {
    pub fn new(
        id: NodeId,
        inbound_receiver: Receiver<Message>,
        outbound_sender: Sender<NetCommand>,
        difficulty: u8,
        connections: Arc<Mutex<Connections>>,
        socker_dir: PathBuf,
    ) -> Self {
        let blockchain = BlockChain::new(difficulty);

        Node {
            id,
            blockchain,
            inbound_receiver,
            outbound_sender,
            longest_peer_chain: (id, 1),
            connections,
            socker_dir,
        }
    }

    pub async fn run_main_process(&mut self) {
        use Message::*;
        use NetCommand::*;
        use UserCommand::*;

        while let Some(cmd) = self.inbound_receiver.recv().await {
            match cmd {
                // User commands
                UCommand(EchoTo { peer_id, message }) => {
                    self.handle_echo_to(peer_id, message).await
                }
                UCommand(SyncWith { peer_id }) => self.handle_sync_with(peer_id).await,
                UCommand(Mine) => self.handle_mining(),
                UCommand(MakeTransaction { from, to, amount }) => {
                    self.handle_add_transaction(from, to, amount)
                }
                UCommand(GetBalance { account_id }) => {
                    self.handle_get_balance(account_id).await;
                }
                // Network commands
                NCommand(EchoToReq { from, message, .. }) => {
                    self.handle_echo_to_req(from, message).await
                }
                NCommand(EchoToRes { message, .. }) => self.handle_echo_to_res(message).await,
                NCommand(ChainLengthReq { to, .. }) => self.handle_chain_length_req(to).await,
                NCommand(ChainLengthRes {
                    from, chain_length, ..
                }) => self.handle_chain_length_res(from, chain_length).await,
                NCommand(SyncWithReq { from, .. }) => self.handle_sync_with_req(from).await,
                NCommand(SyncWithRes {
                    from,
                    known_peers,
                    chain,
                    ..
                }) => self.handle_sync_with_res(from, known_peers, chain).await,
            }

            if self.longest_peer_chain.1 > self.blockchain.chain().len() {
                let peer_id = self.longest_peer_chain.0;
                let request = SyncWithReq {
                    from: self.id,
                    to: peer_id,
                };

                self.outbound_sender.send(request).await.unwrap();
            }
        }
    }

    async fn handle_get_balance(&self, account_id: AccountId) {
        let balances = if self.blockchain.pending.current_balances.is_empty() {
            &self.blockchain.last_block().payload.balances
        } else {
            &self.blockchain.pending.current_balances
        };

        match balances.get(&account_id) {
            Some(b) => {
                println!("+>> Balance: {}", b);
            }
            None => {
                println!("+>> Couldn't find account with id `{}`", account_id.0);
            }
        }
    }

    async fn handle_echo_to(&self, peer_id: NodeId, message: String) {
        use NetCommand::EchoToReq;

        let request = EchoToReq {
            from: self.id,
            to: peer_id,
            message,
        };

        self.outbound_sender.send(request).await.unwrap();
        println!("+>> Request queued ...");
    }

    async fn handle_echo_to_req(&self, peer_id: NodeId, message: String) {
        use NetCommand::EchoToRes;

        println!("+>> Echo: `{}`", &message);

        let response = EchoToRes {
            from: self.id,
            to: peer_id,
            message,
        };

        self.outbound_sender.send(response).await.unwrap();
        println!("+>> Response queued ...");
    }

    async fn handle_echo_to_res(&self, message: String) {
        println!("+>> Echo: `{}`", &message);
    }

    async fn handle_sync_with(&self, peer_id: NodeId) {
        use NetCommand::SyncWithReq;

        let request = SyncWithReq {
            from: self.id,
            to: peer_id,
        };

        self.outbound_sender.send(request).await.unwrap();
        println!("+>> Request queued ...");
    }

    async fn handle_sync_with_req(&self, peer_id: NodeId) {
        use NetCommand::SyncWithRes;

        let connections_guard: MutexGuard<Connections> = self.connections.lock().await;
        let mut known_peers = Vec::new();

        for node_id in connections_guard.0.keys() {
            if !(*node_id == self.id || *node_id == peer_id) {
                known_peers.push(*node_id);
            }
        }

        std::mem::drop(connections_guard);

        let response = SyncWithRes {
            from: self.id,
            to: peer_id,
            known_peers,
            chain: self.blockchain.chain().clone(),
        };

        self.outbound_sender.send(response).await.unwrap();
        println!("+>> Response queued ...");
    }

    async fn handle_sync_with_res(
        &mut self,
        peer_id: NodeId,
        known_peers: Vec<NodeId>,
        chain: Vec<Block>,
    ) {
        use ChainSyncError::*;

        Node::connect_to_peers(
            self.id,
            known_peers,
            self.socker_dir.clone(),
            &mut Arc::clone(&self.connections),
        )
        .await;

        match self.blockchain.update_chain(chain) {
            Ok(()) => {
                println!(
                    "+>> Local blockchain has been synchronized with node `{}`",
                    peer_id.0
                );
            }
            Err(InvalidRemoteCopy) => {
                println!(
                    "+>> WARNING: Remote copy of blockchain isn't valid. Can't sync blockchain with node `{}`",
                    peer_id.0
                );

                Node::destroy_connection(peer_id, self.connections.lock().await).await;

                println!("+>> WARNING: Connecion with node `{}` destroyed", peer_id.0);
            }
            Err(RemoteCopyNotLongerThanLocal) => {
                println!("+>> Remote copy of blockchain isn't longer than local.");
            }
        }
    }

    async fn handle_chain_length_req(&self, peer_id: NodeId) {
        use NetCommand::ChainLengthRes;

        let chain_length = self.blockchain.chain().len();
        let response = ChainLengthRes {
            from: self.id,
            to: peer_id,
            chain_length,
        };

        self.outbound_sender.send(response).await.unwrap();
    }

    async fn handle_chain_length_res(&mut self, peer_id: NodeId, chain_length: usize) {
        if chain_length > self.longest_peer_chain.1 {
            println!(
                "+>> Longer chain detected in node `{}`. Length: {}",
                peer_id.0, chain_length
            );

            self.longest_peer_chain.0 = peer_id;
            self.longest_peer_chain.1 = chain_length;
        }
    }

    fn handle_add_transaction(&mut self, from: AccountId, to: AccountId, amount: u32) {
        use TransactionError::*;

        let timestamp: DateTime<Utc> = Utc::now();
        let tx = Transaction {
            from,
            to,
            amount,
            timestamp,
        };

        match self.blockchain.add_new_transaction(tx) {
            Ok(_) => {
                println!("+>> Transaction of `{}` coin(s) from account `{}` to account `{}` performed successfully", amount, from.0, to.0);
            }
            Err(NonexistentFromAccount) => {
                println!("+>> Account `{}` doesn't exist", from.0);
            }
            Err(NonexistentToAccount) => {
                println!("+>> Account `{}` doesn't exist", to.0);
            }
            Err(InsufficientBalance) => {
                println!(
                    "+>> Account `{}` doesn't have enough balance to perform transaction",
                    from.0
                );
            }
            Err(InvalidAmount) => {
                println!("+>> Amount must be between 1 and {}", MAX_COINS);
            }
            Err(SelfTransaction) => {
                println!("+>> Accounts `from` and `to` cannot be the same");
            }
        }
    }

    fn handle_mining(&mut self) {
        let was_block_added = self.blockchain.mine();

        if was_block_added {
            let chain_length = self.blockchain.chain().len();
            let longest_peer_chain = self.longest_peer_chain.1;

            if chain_length > longest_peer_chain {
                self.longest_peer_chain.0 = self.id;
                self.longest_peer_chain.1 = chain_length;
            }

            let last_block_id = self.blockchain.last_block().id().0;

            println!(
                "+>> Mining sucessfully processed: a new block with id {} has been added",
                last_block_id
            );
        } else {
            println!("+>> WARNING: Empty pool of pending transactions");
        }
    }

    pub async fn run_consensus_process(
        self_node_id: NodeId,
        connections: Arc<Mutex<Connections>>,
        mut sender: Sender<Message>,
    ) {
        use tokio::time::{sleep, Duration};

        loop {
            let connections_guard: MutexGuard<Connections> = connections.lock().await;

            Node::sync_longest_peer_chain(self_node_id, connections_guard, &mut sender).await;
            sleep(Duration::from_millis(POLL_DELAY)).await;
        }
    }

    async fn sync_longest_peer_chain<'a>(
        self_node_id: NodeId,
        connections: MutexGuard<'a, Connections>,
        sender: &mut Sender<Message>,
    ) {
        use Message::NCommand;
        use NetCommand::ChainLengthReq;

        for peer_id in connections.0.keys() {
            let request = NCommand(ChainLengthReq {
                from: self_node_id,
                to: *peer_id,
            });

            sender.send(request).await.unwrap();
        }
    }

    pub async fn reader_process(
        inbound_sender: Sender<Message>,
        rd: ReadHalf<UnixStream>,
        peer_id: NodeId,
        connections: Arc<Mutex<Connections>>,
    ) {
        let length_delimited = FramedRead::new(rd, LengthDelimitedCodec::new());

        let mut serialized: Deserializer = tokio_serde::SymmetricallyFramed::new(
            length_delimited,
            SymmetricalJson::<Value>::default(),
        );

        while let Some(cmd) = serialized.try_next().await.unwrap() {
            let msg: NetCommand = serde_json::from_value(cmd).unwrap();
            inbound_sender.send(Message::NCommand(msg)).await.unwrap();
        }

        println!("+>> node `{}` disconnected ...", peer_id.0);

        Node::destroy_connection(peer_id, connections.lock().await).await;

        println!("+>> Connection to node `{}` removed ...", peer_id.0);
    }

    pub async fn destroy_connection<'a>(
        peer_id: NodeId,
        mut connections: MutexGuard<'a, Connections>,
    ) {
        match connections.0.get_mut(&peer_id) {
            Some(conn) => {
                conn.0.get_mut().get_mut().shutdown().await.unwrap();
                conn.1.abort();
            }
            None => {
                println!("+>> Connection to node `{}` doesn't exist", peer_id.0);
            }
        }

        connections.0.remove(&peer_id);
    }

    pub fn make_node_file_path(dir_path: &str, node_id: NodeId) -> PathBuf {
        let mut file_name = String::from("node.");
        file_name.push_str(node_id.0.to_string().as_str());

        Path::new(dir_path).join(&file_name)
    }

    pub fn create_serializer(wr: WriteHalf<UnixStream>) -> Serializer {
        let length_delimited = FramedWrite::new(wr, LengthDelimitedCodec::new());

        tokio_serde::SymmetricallyFramed::new(length_delimited, SymmetricalJson::default())
    }

    pub async fn create_connection(
        node_addr: PathBuf,
    ) -> (ReadHalf<UnixStream>, WriteHalf<UnixStream>) {
        let socket = UnixStream::connect(node_addr).await.unwrap();
        let (rd, wr): (ReadHalf<UnixStream>, WriteHalf<UnixStream>) = io::split(socket);

        (rd, wr)
    }

    pub async fn run_outbound_queue(
        mut outbound_receiver: Receiver<NetCommand>,
        connections: Arc<Mutex<Connections>>,
    ) {
        while let Some(cmd) = outbound_receiver.recv().await {
            send_message(get_net_cmd_id(&cmd), cmd, connections.lock().await).await;
        }
    }

    pub async fn run_listener(node_addr: &PathBuf, connections: &Arc<Mutex<Connections>>) {
        let listener = UnixListener::bind(node_addr.as_path()).unwrap();
        let connections = Arc::clone(&connections);

        println!("Listening to incoming connections ...");

        loop {
            let (socket, _) = listener.accept().await.unwrap();
            let pid: i32 = socket
                .peer_cred()
                .ok()
                .and_then(|creds| creds.pid())
                .expect("Couldn't get pid from peer connection");
            let peer_id = node_id_from_pid(pid);

            println!("Received connection from node `{}`", peer_id.0);

            let (rd, wr): (ReadHalf<UnixStream>, WriteHalf<UnixStream>) = io::split(socket);
            let reader_process_connections = Arc::clone(&connections);
            let mut connections_guard: MutexGuard<Connections> = connections.lock().await;
            let sender = connections_guard.1.clone();
            let task_handle: JoinHandle<()> = tokio::spawn(async move {
                Node::reader_process(sender, rd, peer_id, reader_process_connections).await;
            });

            connections_guard
                .0
                .insert(peer_id, (Node::create_serializer(wr), task_handle));
        }
    }

    pub async fn connect_to_peers(
        self_node_id: NodeId,
        peer_ids: Vec<NodeId>,
        socker_dir: PathBuf,
        connections: &mut Arc<Mutex<Connections>>,
    ) {
        for peer_id in peer_ids {
            let peer_addr = Node::make_node_file_path(socker_dir.to_str().unwrap(), peer_id);
            let mut connections_guard: MutexGuard<Connections> = connections.lock().await;

            match (peer_id == self_node_id, peer_addr.exists()) {
                (true, _) => {
                    println!("+>> Node can't connect to itself ...");
                }
                (_, false) => {
                    println!("+>> Node `{}` is not active", peer_id.0);
                }
                (false, true) => {
                    if connections_guard.0.contains_key(&peer_id) {
                        println!("+>> Already connected to node `{}`", peer_id.0);
                    } else {
                        println!("+>> Connecting to node `{}`", peer_id.0);

                        let sender = connections_guard.1.clone();
                        let (rd, wr) = Node::create_connection(peer_addr).await;
                        let reader_process_connections = Arc::clone(&connections);
                        let task_handle: JoinHandle<()> = tokio::spawn(async move {
                            Node::reader_process(sender, rd, peer_id, reader_process_connections)
                                .await;
                        });

                        connections_guard
                            .0
                            .insert(peer_id, (Node::create_serializer(wr), task_handle));
                    }
                }
            }

            std::mem::drop(connections_guard);
        }
    }
}

async fn send_message<'a>(
    peer_id: NodeId,
    cmd: NetCommand,
    mut connections: MutexGuard<'a, Connections>,
) {
    let val = serde_json::to_value(cmd).unwrap();

    match connections.0.get_mut(&peer_id) {
        Some((wr, _)) => {
            wr.send(val).await.unwrap();
        }
        None => {
            println!("+>> Connection to node `{}` doesn't exist", peer_id.0);
        }
    }
}

fn node_id_from_pid(pid: i32) -> NodeId {
    let pid = pid.to_string();
    let mut cmd = String::new();

    cmd.push_str("lsof -p ");
    cmd.push_str(pid.as_str());
    cmd.push_str(" | awk '{print $9}' | grep _rusty_nodes");

    let output = ProcCommand::new("sh")
        .arg("-c")
        .arg(cmd.as_str())
        .output()
        .unwrap();
    let mut output = String::from_utf8(output.stdout).unwrap();

    output.pop();

    let path = Path::new(&output);
    let extension = path
        .extension()
        .and_then(|ext| ext.to_str())
        .expect("Couldn't parse node's id");

    NodeId(extension.parse().expect("Couldn't parse node's id"))
}

fn get_net_cmd_id(cmd: &NetCommand) -> NodeId {
    use NetCommand::*;

    match cmd {
        EchoToReq { to, .. }
        | EchoToRes { to, .. }
        | SyncWithReq { to, .. }
        | SyncWithRes { to, .. }
        | ChainLengthReq { to, .. }
        | ChainLengthRes { to, .. } => *to,
    }
}
