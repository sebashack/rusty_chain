#[macro_use]
extern crate clap;

use clap::{App, Arg};
use lazy_static::lazy_static;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use tokio::signal;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::{Mutex, MutexGuard};
use tokio::{io, io::AsyncBufReadExt, io::AsyncWriteExt, io::BufReader};

use blockchain::blockchain::AccountId;
use blockchain::node::{Connections, Message, NetCommand, Node, NodeId, UserCommand};

lazy_static! {
    pub static ref SOCKET_DIR: PathBuf = {
        let home_dir = dirs::home_dir().expect("Couldn't get path to home directory");
        home_dir.as_path().join("_rusty_nodes")
    };
}

#[tokio::main]
async fn main() {
    let matches = App::new("rusty_chain")
        .version("1.0")
        .arg(
            Arg::with_name("NODE_ID")
                .short("n")
                .long("node")
                .help("Sets the unique identifier for this node")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    let node_id = value_t!(matches.value_of("NODE_ID"), u16)
        .expect("node_id must be an integer between 0 and 65536");
    let node_id = NodeId(node_id);
    let node_file_path_buf =
        move || Node::make_node_file_path(SOCKET_DIR.to_str().unwrap(), node_id);

    if node_file_path_buf().exists() {
        panic!("node_id `{}` is already taken", node_id.0);
    }

    if !SOCKET_DIR.is_dir() {
        std::fs::create_dir(SOCKET_DIR.as_path()).unwrap();
    }

    let (inbound_sender, inbound_receiver): (Sender<Message>, Receiver<Message>) = channel(32);
    let client_sender = (&inbound_sender).clone();
    let consensus_sender = (&inbound_sender).clone();
    let (outbound_sender, outbound_receiver): (Sender<NetCommand>, Receiver<NetCommand>) =
        channel(32);
    let connections: Arc<Mutex<Connections>> =
        Arc::new(Mutex::new((HashMap::new(), inbound_sender)));
    let mut node: Node = Node::new(
        node_id,
        inbound_receiver,
        outbound_sender.clone(),
        14,
        Arc::clone(&connections),
        SOCKET_DIR.to_path_buf(),
    );

    // Node tasks
    tokio::spawn(async move {
        node.run_main_process().await;
    });

    let consensus_connections = Arc::clone(&connections);
    tokio::spawn(async move {
        Node::run_consensus_process(node_id, consensus_connections, consensus_sender).await;
    });

    let node_connections = Arc::clone(&connections);
    tokio::spawn(async move {
        Node::run_outbound_queue(outbound_receiver, node_connections).await;
    });

    // Client task
    let mut client_connections = Arc::clone(&connections);

    tokio::spawn(async move {
        use Message::UCommand;
        use UserCommand::*;

        let stdin = io::stdin();
        let reader = BufReader::new(stdin);
        let mut lines = reader.lines();

        // Client task
        while let Some(line) = lines.next_line().await.unwrap() {
            let self_node_id = node_id;
            let words: Vec<&str> = line.split_whitespace().collect();
            let words: Vec<&str> = words.iter().map(|&s| s.trim()).collect();

            match words.as_slice() {
                ["ConnectTo", peer_id] => match peer_id.parse() {
                    Ok(peer_id) => {
                        Node::connect_to_peers(
                            self_node_id,
                            vec![NodeId(peer_id)],
                            SOCKET_DIR.to_path_buf(),
                            &mut client_connections,
                        )
                        .await;
                    }
                    Err(_) => {
                        println!("+>> Couldn't parse node_id `{}`", peer_id);
                    }
                },
                ["EchoTo", peer_id, message] => match peer_id.parse() {
                    Ok(peer_id) => {
                        client_sender
                            .send(UCommand(EchoTo {
                                peer_id: NodeId(peer_id),
                                message: message.to_string(),
                            }))
                            .await
                            .unwrap();
                    }
                    Err(_) => {
                        println!("+>> Couldn't parse node_id `{}`", peer_id);
                    }
                },
                ["SyncWith", peer_id] => match peer_id.parse() {
                    Ok(peer_id) => {
                        client_sender
                            .send(UCommand(SyncWith {
                                peer_id: NodeId(peer_id),
                            }))
                            .await
                            .unwrap();
                    }
                    Err(_) => {
                        println!("+>> Couldn't parse node_id `{}`", peer_id);
                    }
                },
                ["MakeTransaction", from, to, amount] => {
                    match (from.parse(), to.parse(), amount.parse()) {
                        (Ok(from), Ok(to), Ok(amount)) => {
                            let from = AccountId(from);
                            let to = AccountId(to);

                            client_sender
                                .send(UCommand(MakeTransaction { from, to, amount }))
                                .await
                                .unwrap();
                        }
                        _ => {
                            println!("+>> Couldn't parse transaction. Format is `MakeTransaction [from] [to] [amount]`");
                        }
                    }
                }
                ["Mine"] => {
                    client_sender.send(UCommand(Mine)).await.unwrap();
                }
                ["GetBalance", acc] => match acc.parse() {
                    Ok(acc) => client_sender
                        .send(UCommand(GetBalance {
                            account_id: AccountId(acc),
                        }))
                        .await
                        .unwrap(),
                    Err(_) => {
                        println!("+>> Couldn't parse account_id `{}`", acc);
                    }
                },
                ["Disconnect"] => {
                    println!("+>> Disconnecting ...");
                    clean_up(node_id, client_connections.lock().await).await;
                }
                _ => println!("+>> Invalid command"),
            }
        }
    });

    let ctrl_c_connections = Arc::clone(&connections);
    tokio::spawn(async move {
        signal::ctrl_c().await.unwrap();
        clean_up(node_id, ctrl_c_connections.lock().await).await;
    });

    // Listener task
    Node::run_listener(&node_file_path_buf(), &connections).await;
}

async fn clean_up<'a>(node_id: NodeId, mut connections: MutexGuard<'a, Connections>) {
    for (node_id, conn) in connections.0.iter_mut() {
        println!("+>> Removing connection with node `{}`", node_id.0);

        conn.0.get_mut().get_mut().shutdown().await.unwrap();
        conn.1.abort();
    }

    let node_path = Node::make_node_file_path(SOCKET_DIR.to_str().unwrap(), node_id);
    remove_file_path_and_exit(&node_path, &node_id);
}

fn remove_file_path_and_exit(path: &Path, node_id: &NodeId) {
    if path.exists() {
        println!("Remove socket file for node-{}", node_id.0);
        std::fs::remove_file(path).unwrap();
    }

    std::process::exit(0);
}
