use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha512};
use std::cmp::Ordering;
use std::collections::{BTreeMap, BTreeSet};

pub static NUM_ACCOUNTS: u32 = 10;
pub static MAX_COINS: u32 = 100;

pub type SHA512 = Vec<u8>;

#[derive(Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy)]
pub struct AccountId(pub u64);

#[derive(Serialize, Deserialize, PartialEq, Eq, Ord, Debug, Clone)]
pub struct Transaction {
    pub from: AccountId,
    pub to: AccountId,
    pub amount: u32,
    pub timestamp: DateTime<Utc>,
}

impl PartialOrd for Transaction {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.timestamp.partial_cmp(&other.timestamp)
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy)]
pub struct BlockId(pub usize);

#[derive(Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
pub struct BlockHash(pub SHA512);

type Nonce = (usize, usize);
#[derive(Serialize, Deserialize, Eq, Debug, Clone)]
pub struct BlockPayload {
    id: BlockId,
    transactions: BTreeSet<Transaction>,
    pub balances: BTreeMap<AccountId, u32>,
    timestamp: DateTime<Utc>,
    pub previous_hash: BlockHash,
    nonce: Nonce,
}

impl BlockPayload {
    pub fn new(
        id: BlockId,
        transactions: BTreeSet<Transaction>,
        balances: BTreeMap<AccountId, u32>,
        timestamp: DateTime<Utc>,
        previous_hash: BlockHash,
    ) -> Self {
        BlockPayload {
            id,
            transactions,
            balances,
            timestamp,
            previous_hash,
            nonce: (0, 0),
        }
    }

    pub fn compute_hash(&self) -> BlockHash {
        let payload = serde_json::to_string(&self).unwrap();
        compute_block_hash(payload.as_bytes())
    }
}

impl PartialEq for BlockPayload {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
            && self.transactions == other.transactions
            && self.balances == other.balances
            && self.timestamp == other.timestamp
    }
}

#[derive(Serialize, Deserialize, Eq, Debug, Clone)]
pub struct Block {
    pub payload: BlockPayload,
    pub hash: BlockHash,
}

impl PartialEq for Block {
    fn eq(&self, other: &Self) -> bool {
        self.payload == other.payload && self.hash == other.hash
    }
}

impl Block {
    pub fn new(payload: BlockPayload, hash: BlockHash) -> Self {
        Block { payload, hash }
    }

    pub fn id(&self) -> BlockId {
        self.payload.id
    }

    pub fn timestamp(&self) -> &DateTime<Utc> {
        &self.payload.timestamp
    }

    pub fn previous_hash(&self) -> &BlockHash {
        &self.payload.previous_hash
    }

    pub fn transactions(&self) -> &BTreeSet<Transaction> {
        &self.payload.transactions
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum ChainSyncError {
    RemoteCopyNotLongerThanLocal,
    InvalidRemoteCopy,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum TransactionError {
    NonexistentFromAccount,
    NonexistentToAccount,
    InsufficientBalance,
    InvalidAmount,
    SelfTransaction,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PendingTransactions {
    pub current_balances: BTreeMap<AccountId, u32>,
    transactions: BTreeSet<Transaction>,
}

impl PendingTransactions {
    pub fn new(
        current_balances: BTreeMap<AccountId, u32>,
        transactions: BTreeSet<Transaction>,
    ) -> Self {
        PendingTransactions {
            current_balances,
            transactions,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BlockChain {
    chain: Vec<Block>,
    pub pending: PendingTransactions,
    difficulty: u8, // Difficulty must be an u8 between 1 and 80
}

impl BlockChain {
    pub fn new(difficulty: u8) -> Self {
        let genesis_block = create_genesis_block(difficulty);
        let number_of_zeroes = match difficulty {
            n if n < 1 => 1,
            n if n > 80 => 80,
            _ => difficulty,
        };
        let mut chain = Vec::new();

        chain.push(genesis_block);

        BlockChain {
            chain,
            difficulty: number_of_zeroes,
            pending: PendingTransactions::new(BTreeMap::new(), BTreeSet::new()),
        }
    }

    pub fn last_block(&self) -> &Block {
        self.chain.last().unwrap()
    }

    pub fn last_block_mut(&mut self) -> &mut Block {
        self.chain.last_mut().unwrap()
    }

    pub fn chain(&self) -> &Vec<Block> {
        &self.chain
    }

    pub fn chain_mut(&mut self) -> &mut Vec<Block> {
        &mut self.chain
    }

    pub fn update_chain(&mut self, other_chain: Vec<Block>) -> Result<(), ChainSyncError> {
        if other_chain.len() <= self.chain.len() {
            return Err(ChainSyncError::RemoteCopyNotLongerThanLocal);
        }

        if !BlockChain::is_valid_chain(self.difficulty, &other_chain) {
            return Err(ChainSyncError::InvalidRemoteCopy);
        }

        self.chain = other_chain;

        Ok(())
    }

    pub fn add_new_transaction(
        &mut self,
        transaction: Transaction,
    ) -> Result<(), TransactionError> {
        if self.pending.current_balances.is_empty() {
            self.pending.current_balances = self.chain().last().unwrap().payload.balances.clone();
        }

        if transaction.amount < 1 || transaction.amount > MAX_COINS {
            return Err(TransactionError::InsufficientBalance);
        }

        if transaction.from == transaction.to {
            return Err(TransactionError::SelfTransaction);
        }

        let from = &transaction.from;
        let to = &transaction.to;
        let current_balances = &mut self.pending.current_balances;

        match (current_balances.get(from), current_balances.get(to)) {
            (Some(balance_from), Some(balance_to)) => {
                if *balance_from >= transaction.amount {
                    let new_balance_from = balance_from - transaction.amount;
                    let new_balance_to = balance_to + transaction.amount;

                    current_balances.insert(*from, new_balance_from);
                    current_balances.insert(*to, new_balance_to);
                } else {
                    return Err(TransactionError::InsufficientBalance);
                }
            }
            (_, None) => {
                return Err(TransactionError::NonexistentToAccount);
            }
            (None, _) => {
                return Err(TransactionError::NonexistentFromAccount);
            }
        }

        self.pending.transactions.insert(transaction);

        Ok(())
    }

    pub fn mine(&mut self) -> bool {
        if self.pending.transactions.is_empty() {
            return false;
        }

        let now: DateTime<Utc> = Utc::now();
        let previous_hash = self.last_block().hash.clone();
        let transactions = std::mem::replace(&mut self.pending.transactions, BTreeSet::new());
        let balances = std::mem::replace(&mut self.pending.current_balances, BTreeMap::new());
        let mut new_payload = BlockPayload::new(
            BlockId(self.last_block().payload.id.0 + 1),
            transactions,
            balances,
            now,
            previous_hash,
        );
        let proof = proof_of_work(self.difficulty, &mut new_payload);
        let new_block = Block::new(new_payload, proof);

        self.add_block(new_block)
    }

    fn add_block(&mut self, block: Block) -> bool {
        let previous_hash = &self.last_block().hash;

        if previous_hash != &block.payload.previous_hash {
            return false;
        }

        if !is_valid_proof(self.difficulty, &block, &block.hash) {
            return false;
        }

        self.chain.push(block);

        true
    }

    fn is_valid_chain(difficulty: u8, other_chain: &Vec<Block>) -> bool {
        let mut previous_hash = compute_block_hash(GENESIS_HASH_PAYLOAD);

        for block in other_chain {
            let other_block_hash = &block.hash;

            if previous_hash != block.payload.previous_hash {
                return false;
            }

            if !is_valid_proof(difficulty, block, &other_block_hash) {
                return false;
            }

            previous_hash = other_block_hash.clone();
        }

        true
    }
}

// Helpers
const GENESIS_HASH_PAYLOAD: &'static [u8; 12] = b"genesis_hash";

fn create_genesis_block(difficulty: u8) -> Block {
    let utc: DateTime<Utc> = Utc::now();
    let previous_hash = compute_block_hash(GENESIS_HASH_PAYLOAD);
    let mut balances = BTreeMap::new();
    let coins_per_account = MAX_COINS / NUM_ACCOUNTS;

    for i in 0..NUM_ACCOUNTS {
        balances.insert(AccountId(i.into()), coins_per_account);
    }

    let mut payload = BlockPayload::new(BlockId(0), BTreeSet::new(), balances, utc, previous_hash);
    let hash = proof_of_work(difficulty, &mut payload);

    Block::new(payload, hash)
}

fn is_valid_proof(difficulty: u8, block: &Block, hash: &BlockHash) -> bool {
    starts_with_n_zeroes(hash, difficulty) && &block.payload.compute_hash() == hash
}

fn compute_block_hash(payload: &[u8]) -> BlockHash {
    let mut hasher = Sha512::new();

    hasher.update(payload);

    let result = hasher.finalize();

    BlockHash(result.as_slice().to_vec())
}

fn proof_of_work(difficulty: u8, block_payload: &mut BlockPayload) -> BlockHash {
    let mut computed_hash = block_payload.compute_hash();
    let mut hash_was_found = false;

    for i in 0..std::usize::MAX {
        for j in 0..std::usize::MAX {
            block_payload.nonce = (i, j);

            computed_hash = block_payload.compute_hash();

            if starts_with_n_zeroes(&computed_hash, difficulty) {
                hash_was_found = true;
                break;
            };
        }

        if hash_was_found {
            break;
        };
    }

    computed_hash
}

fn starts_with_n_zeroes(hash: &BlockHash, n: u8) -> bool {
    if n == 0 {
        return true;
    }

    let mut consecutive_zeroes = 0;
    let mut non_zero_found = false;

    for i in 0..(n as usize) {
        let byte_array = byte_into_bit_array(hash.0[i]);

        for b in &byte_array {
            if *b == 0 {
                consecutive_zeroes = consecutive_zeroes + 1;
            } else {
                non_zero_found = true;
                break;
            }
        }

        if consecutive_zeroes >= n {
            return true;
        }

        if non_zero_found {
            break;
        }
    }

    false
}

fn byte_into_bit_array(byte: u8) -> [u8; 8] {
    let mut arr: [u8; 8] = [0; 8];

    for i in 0..8 {
        let bit = 0x1 & (byte >> 7 - i);

        arr[i] = bit;
    }

    arr
}

#[cfg(test)]
mod tests {
    use crate::blockchain::{byte_into_bit_array, starts_with_n_zeroes, BlockHash};

    #[test]
    fn byte_into_bit_array_should_produce_correct_bit_sequence() {
        assert!(byte_into_bit_array(0x1) == [0, 0, 0, 0, 0, 0, 0, 1]);
        assert!(byte_into_bit_array(0x7F) == [0, 1, 1, 1, 1, 1, 1, 1]);
        assert!(byte_into_bit_array(0xF) == [0, 0, 0, 0, 1, 1, 1, 1]);
        assert!(byte_into_bit_array(0xAA) == [1, 0, 1, 0, 1, 0, 1, 0]);
        assert!(byte_into_bit_array(0x55) == [0, 1, 0, 1, 0, 1, 0, 1]);
        assert!(byte_into_bit_array(0xFF) == [1, 1, 1, 1, 1, 1, 1, 1]);
        assert!(byte_into_bit_array(0xC3) == [1, 1, 0, 0, 0, 0, 1, 1]);
        assert!(byte_into_bit_array(0x3C) == [0, 0, 1, 1, 1, 1, 0, 0]);
    }

    #[test]
    fn starts_with_n_zeroes_should_compute_right_answer() {
        let mut hash_with_1_zero = BlockHash(vec![0x7F]);

        for _ in 1..64 {
            hash_with_1_zero.0.push(0xFF);
        }

        assert!(starts_with_n_zeroes(&hash_with_1_zero, 1));
        assert!(!starts_with_n_zeroes(&hash_with_1_zero, 2));

        let mut hash_with_2_zeroes = BlockHash(vec![0x3F]);

        for _ in 1..64 {
            hash_with_2_zeroes.0.push(0xFF);
        }

        assert!(starts_with_n_zeroes(&hash_with_2_zeroes, 2));
        assert!(!starts_with_n_zeroes(&hash_with_2_zeroes, 3));

        let mut hash_with_8_zeroes = BlockHash(vec![0x0]);

        for _ in 1..64 {
            hash_with_8_zeroes.0.push(0xFF);
        }

        assert!(starts_with_n_zeroes(&hash_with_8_zeroes, 8));
        assert!(!starts_with_n_zeroes(&hash_with_8_zeroes, 9));

        let mut hash_with_12_zeroes = BlockHash(vec![0x0, 0xF]);

        for _ in 2..64 {
            hash_with_12_zeroes.0.push(0xFF);
        }

        assert!(starts_with_n_zeroes(&hash_with_12_zeroes, 12));
        assert!(!starts_with_n_zeroes(&hash_with_12_zeroes, 13));

        let mut hash_with_26_zeroes = BlockHash(vec![0x0, 0x0, 0x0, 0x3F]);

        for _ in 3..64 {
            hash_with_26_zeroes.0.push(0xFF);
        }

        assert!(starts_with_n_zeroes(&hash_with_26_zeroes, 26));
        assert!(!starts_with_n_zeroes(&hash_with_26_zeroes, 27));

        let hash_with_8_zeroes_in_between = BlockHash(vec![0xFF, 0xFF, 0x0, 0xFF]);
        assert!(!starts_with_n_zeroes(&hash_with_8_zeroes_in_between, 8));
    }
}
