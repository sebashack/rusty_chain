#!/bin/bash

set -eo pipefail

sudo apt-get update
sudo apt install build-essential
sudo apt-get install -y curl

# Install Rust compiler
if ! type "rustup" > /dev/null; then
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs |  sh -s -- -y
else
    echo 'rustup already installed'
fi

source $HOME/.cargo/env

rustup install 1.49.0
rustup default 1.49.0
rustup component add rustfmt
