# Rusty chain

Blockchain naive simulation in unix-sockets.

## Operating system

This project is intended to work only on Ubuntu 18.04 and 20.04 platforms.

## First time install

To install all the dependencies to compile the project run the following:

```
./first-time-install.sh
```

## Compile release binaries

```
cargo build --release
```

## Run a node

```
cargo run --release --bin node -- -n <node-id>
```

where `node-id` is a natural number that uniquely identifies a node.
